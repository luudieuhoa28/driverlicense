package com.example.driverlicense.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.driverlicense.database.QuestionDatabaseAccess;
import com.example.driverlicense.entity.Question;
import com.example.driverlicense.entity.TestQuestion;

import java.util.ArrayList;
import java.util.List;

public class QuestionViewModel extends AndroidViewModel {
    List<Question> listQuest = new ArrayList<>();
    QuestionDatabaseAccess questionDatabaseAccess;
    //them 1 casi gif dos ddeer moc tuwf db leen danh danh sach cau hoi theo id Test
    // List<Question> getListQuestTestFromDb(List<QuestionTest> listQuestionTest);
    public QuestionViewModel(@NonNull Application application) {
        super(application);
//        listQuest.add(new Question(1, "Chon dap an dii", "", "A nef", "B nef", "C nef", "", "Cau nay cau a dung nha", 1, false, 0, false));
//        listQuest.add(new Question(2, "Chon dap an dii heeh", "", "A nef", "B nef", "C nef", "", "Cau nay cau b dung nha", 2, false, 0, false));
//        listQuest.add(new Question(3, "Chon dap an dii hoho", "", "A nef", "B nef", "C nef", "D nef", "Cau nay cau c dung nha", 3, false, 0, false));
//        listQuest.add(new Question(4, "Chon dap an dii hohohksjhfjsf", "", "A nef", "B nef", "C nef", "D nef", "Cau nay cau c dung nha", 3, false, 0, false));
        questionDatabaseAccess = QuestionDatabaseAccess.getInstance(application.getApplicationContext());
    }

    public List<Question> getListQuest(List<TestQuestion> testQuestionList) {
        questionDatabaseAccess.open();
        listQuest = questionDatabaseAccess.getListQuestion(testQuestionList);
        questionDatabaseAccess.close();
        return listQuest;
    }

    public void setListQuest(List<Question> listQuest) {
        this.listQuest = listQuest;
    }
}
