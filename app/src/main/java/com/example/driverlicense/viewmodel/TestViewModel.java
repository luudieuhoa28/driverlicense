package com.example.driverlicense.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.driverlicense.database.TestDatabaseAccess;
import com.example.driverlicense.entity.Question;
import com.example.driverlicense.entity.Test;

import java.util.ArrayList;
import java.util.List;

public class TestViewModel extends AndroidViewModel {

    private List<Test> listTest = new ArrayList<>();
    TestDatabaseAccess testDatabaseAccess;
    //them 1 cai gi do de lay database len
    //List<Test> getListTest();
    public TestViewModel(@NonNull Application application) {
        super(application);
//        listTest = new ArrayList<>();
//        listTest.add(new Test(1,1, 0, 0, false, false));
//        listTest.add(new Test(2, 2, 0, 0, false, false));
//        listTest.add(new Test(3, 3, 0, 0, false, false));
//        listTest.add(new Test(4, 4, 0, 0, false, false));
//        listTest.add(new Test(5, 5, 0, 0, false, false));
//        listTest.add(new Test(6, 6, 0, 0, false, false));
//        listTest.add(new Test(7,7, 0, 0, false, false));
        testDatabaseAccess = TestDatabaseAccess.getInstance(application.getApplicationContext());

    }
    public List<Test> getListTest() {
        testDatabaseAccess.open();
        listTest = testDatabaseAccess.getListTest("A1");

        testDatabaseAccess.close();
        return listTest;
    }

    public void setListTest(List<Test> listTest) {
        this.listTest = listTest;
    }

    //check if test pass or fail

}
