package com.example.driverlicense.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.driverlicense.activity.DoTestActivity;
import com.example.driverlicense.database.TestQuestionDatabaseAccess;
import com.example.driverlicense.entity.TestQuestion;

import java.util.ArrayList;
import java.util.List;

public class TestQuestionViewModel  extends AndroidViewModel {
    private List<TestQuestion> listTestQuestion = new ArrayList<>();
   TestQuestionDatabaseAccess testQuestionDatabaseAccess;

    //Ở đây tạo 1 hàm gọi xuống db trả về 1 list TestQuest bằng testId
    public TestQuestionViewModel(@NonNull Application application) {
        super(application);
//        testQuestionList.add(new TestQuestion(1, 1, 1, 0, 0, 0));
////        testQuestionList.add(new TestQuestion(1, 1, 2, 0, 0, 0));
////        testQuestionList.add(new TestQuestion(1, 1, 3, 0, 0, 0));
////        testQuestionList.add(new TestQuestion(1, 1, 4, 0, 0, 0));
        testQuestionDatabaseAccess  = TestQuestionDatabaseAccess.getInstance(application.getApplicationContext());

    }

    public List<TestQuestion> getTestQuestionList(int testId) {
        testQuestionDatabaseAccess.open();
        listTestQuestion = testQuestionDatabaseAccess.getTestQuestionById(testId);
        testQuestionDatabaseAccess.close();
        return listTestQuestion;
    }

    public void setTestQuestionList(List<TestQuestion> testQuestionList) {
        this.listTestQuestion = testQuestionList;
    }

    public List<TestQuestion> resetTestQuest() {
        testQuestionDatabaseAccess.open();
        testQuestionDatabaseAccess.resetTestQuest(listTestQuestion);
        testQuestionDatabaseAccess.close();
        return listTestQuestion;
    }
}
