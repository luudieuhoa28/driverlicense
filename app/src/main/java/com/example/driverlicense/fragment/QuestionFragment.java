package com.example.driverlicense.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverlicense.R;
import com.example.driverlicense.adapter.QuestionPagerAdapter;
import com.example.driverlicense.database.TestQuestionDatabaseAccess;
import com.example.driverlicense.entity.Option;
import com.example.driverlicense.entity.Question;
import com.example.driverlicense.adapter.QuestionBeforeTestAdapter;
import com.example.driverlicense.entity.Test;
import com.example.driverlicense.entity.TestQuestion;

import java.util.ArrayList;
import java.util.List;
//show the full question: question content, question options, question image
//handing the choose answer action
public class QuestionFragment extends Fragment {

    private static List<Question> listTestQuestion = new ArrayList<>();

    public static QuestionFragment newInstance(Question question, TestQuestion testQuestion, int position, Test test) {
        QuestionFragment fragment = new QuestionFragment();
        Bundle args = new Bundle();
        args.putSerializable("QUESTION", question);
        args.putSerializable("QUESTION_TEST", testQuestion);
        args.putInt("ORDER_QUESTION", position + 1);
        args.putSerializable("TEST", test);
        fragment.setArguments(args);
        return fragment;
    }

    public static List<Question> getListTestQuestion() {
        return listTestQuestion;
    }

    public static void setListTestQuestion(List<Question> listTestQuestion) {
        QuestionFragment.listTestQuestion = listTestQuestion;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View questionView = inflater.inflate(R.layout.do_test_fragment, container, false);
        TextView questionTextView = questionView.findViewById(R.id.questionTextView);
        ImageView questionImageView = questionView.findViewById(R.id.questionImageView);
        final RecyclerView listAnswer = questionView.findViewById(R.id.listAnswer);
        TextView explainTitle = questionView.findViewById(R.id.txtExplainTitle);
        TextView explainQuestion = questionView.findViewById(R.id.txtExplainQuestion);
        final Question question = (Question) getArguments().getSerializable("QUESTION");
        final TestQuestion testQuestion = (TestQuestion) getArguments().getSerializable("QUESTION_TEST");
        final Test test = (Test) getArguments().getSerializable("TEST");

        if(test.isFinish()) {
            explainTitle.setText("Giải thích đáp án");
            explainQuestion.setText(question.getAnswerDescription());
        } else {
            explainTitle.setText("");
            explainQuestion.setText("");
        }

        questionTextView.setText(question.getQuestionContent());
        if (!question.getQuestionImg().equals("")) {
            //this is for test image display
            Uri otherPath = Uri.parse("android.resource://com.example.driverlicense/drawable/a");
            //  questionImageView.setImageURI("android.resource://com.example.driverlicense/drawable/" + Uri.parse(question.getQuestionImg()));
            questionImageView.setImageURI(otherPath);
        } else {
            questionImageView.setImageResource(0);
        }
        final List<Option> listOption = new ArrayList<>();

//        listOption.add(new Option("A", question.getOption1(), question.getMarked() == 1 ? true : false, false, false));
//        listOption.add(new Option("B", question.getOption2(), question.getMarked() == 2 ? true : false, false, false));
//        listOption.add(new Option("C", question.getOption3(), question.getMarked() == 3 ? true : false, false, false));

        listOption.add(new Option("A", question.getOption1(), testQuestion.getAnswer() == 1 ? true : false, false, false));
        listOption.add(new Option("B", question.getOption2(), testQuestion.getAnswer() == 2 ? true : false, false, false));

        if (question.getOption3() != null) {
            listOption.add(new Option("C", question.getOption3(), testQuestion.getAnswer() == 3 ? true : false, false, false));
        }
        if (question.getOption4() != null) {
            listOption.add(new Option("D", question.getOption4(), testQuestion.getAnswer() == 4 ? true : false, false, false));
        }

        //update when user choose the answer
        final QuestionBeforeTestAdapter questionBeforeTestAdapter = new QuestionBeforeTestAdapter(listOption, question, test, testQuestion);
        questionBeforeTestAdapter.setOnItemClickListener(new QuestionBeforeTestAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(testQuestion.getAnswer() == position + 1){
                    testQuestion.setAnswer(0);
                } else {
                    testQuestion.setAnswer(position + 1);
                }

                for (int i = 0; i < listOption.size(); i++) {
                    if (i == position) {
                        if (listOption.get(i).isSelected()) {
                            listOption.get(i).setSelected(false);
                        } else {
                            listOption.get(i).setSelected(true);
                        }

                    } else {
                        listOption.get(i).setSelected(false);
                    }
                }
//                if(position + 1 == question.getAnswer()) {
//                    question.setWrong(false);
//                } else {
//                    question.setWrong(true);
//                }
                //chỗ này update xuống db
                questionBeforeTestAdapter.notifyItemChanged(position);
                questionBeforeTestAdapter.notifyDataSetChanged();
                TestQuestionDatabaseAccess testQuestionDatabaseAccess = TestQuestionDatabaseAccess.getInstance(getContext());
                testQuestionDatabaseAccess.open();
                testQuestionDatabaseAccess.updateTestQuest(testQuestion);
                testQuestionDatabaseAccess.close();

                RecyclerView listView = (RecyclerView) getActivity().findViewById(R.id.listStatusQuestion);
                if (listView.getAdapter() != null) {
                    listView.getAdapter().notifyDataSetChanged();
                }

            }
        });
        listAnswer.setLayoutManager(new LinearLayoutManager(getActivity()));
        listAnswer.setAdapter(questionBeforeTestAdapter);
     //   ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getArguments().getInt("ORDER_QUESTION") + "/25");
//        TextView pageNumber = ((AppCompatActivity) getActivity()).findViewById(R.id.txtOrderQuest);
//        pageNumber.setText(getArguments().getInt("ORDER_QUESTION") + "/25");
        Toast.makeText(this.getContext(),"create view ne", Toast.LENGTH_SHORT).show();
        return questionView;
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("hi");
      //  Toast.makeText(this.getContext(),"Resume fragment", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

}
