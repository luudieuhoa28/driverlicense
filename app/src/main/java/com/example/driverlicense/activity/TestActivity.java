package com.example.driverlicense.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.driverlicense.R;
import com.example.driverlicense.entity.Test;
import com.example.driverlicense.adapter.TestAdapter;
import com.example.driverlicense.viewmodel.TestViewModel;

//display list test sets (bộ đề)
public class TestActivity extends AppCompatActivity {
    public static final int DO_TEST_ACTIVITY = 1;
    public static TestAdapter testAdapter;
    TestViewModel testViewModel;
    RecyclerView testRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test);
        testViewModel = ViewModelProviders.of(this).get(TestViewModel.class);
        testAdapter = new TestAdapter(testViewModel.getListTest());
        testRecyclerView = (RecyclerView) findViewById(R.id.testRecyclerView);
        testRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        testRecyclerView.setAdapter(testAdapter);

        testAdapter.setOnItemClickListener(new TestAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Test test) {
                Intent intent = new Intent(TestActivity.this, DoTestActivity.class);
                Bundle testData = new Bundle();
                testData.putSerializable("TEST_DATA", test);
                intent.putExtra("DATA_BUNDLE", testData);
                startActivityForResult(intent, DO_TEST_ACTIVITY);
            }
        });
        Toast.makeText(this, "create", Toast.LENGTH_SHORT).show();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//    }

    @Override
    protected void onRestart() {
        super.onRestart();
        testAdapter = new TestAdapter(testViewModel.getListTest());
        testRecyclerView.setAdapter(testAdapter);
        testAdapter.setOnItemClickListener(new TestAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Test test) {
                Intent intent = new Intent(TestActivity.this, DoTestActivity.class);
                Bundle testData = new Bundle();
                testData.putSerializable("TEST_DATA", test);
                intent.putExtra("DATA_BUNDLE", testData);
                startActivityForResult(intent, DO_TEST_ACTIVITY);
            }
        });
        //  testAdapter.notifyDataSetChanged();
        Toast.makeText(this, "resume", Toast.LENGTH_SHORT).show();
    }
}