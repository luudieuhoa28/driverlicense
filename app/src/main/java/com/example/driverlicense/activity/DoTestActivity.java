package com.example.driverlicense.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.driverlicense.adapter.TestStatusAdapter;
import com.example.driverlicense.database.TestDatabaseAccess;
import com.example.driverlicense.database.DatabasePreparing;
import com.example.driverlicense.database.TestQuestionDatabaseAccess;
import com.example.driverlicense.entity.Question;
import com.example.driverlicense.adapter.QuestionPagerAdapter;
import com.example.driverlicense.entity.TestQuestion;
import com.example.driverlicense.viewmodel.QuestionViewModel;
import com.example.driverlicense.R;
import com.example.driverlicense.entity.Test;
import com.example.driverlicense.viewmodel.TestQuestionViewModel;

import java.util.List;

public class DoTestActivity extends AppCompatActivity {
    QuestionViewModel questionViewModel;
    private static List<Question> listQuestion;
    private List<TestQuestion> listTestQuestion;
    TestQuestionViewModel testQuestionViewModel;
    ViewPager questionViewPager;
    QuestionPagerAdapter questionPagerAdapter;
    RecyclerView listStatusQuestion;
    TestStatusAdapter testStatusAdapter;
    RecyclerView testList;
    CountDownTimer countDownTimer;
    Test test;
    DoTestActivity doTestActivity;
    boolean isCountTimerRunning = false;
    TextView txtOrderQuest;


    public QuestionViewModel getQuestionViewModel() {
        return questionViewModel;
    }

    public void setQuestionViewModel(QuestionViewModel questionViewModel) {
        this.questionViewModel = questionViewModel;
    }

    public static List<Question> getListQuestion() {
        return listQuestion;
    }

    public static void setListQuestion(List<Question> listQuestion) {
        DoTestActivity.listQuestion = listQuestion;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_test);
        doTestActivity = this;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        Intent intent = getIntent();
        Bundle testData = intent.getBundleExtra("DATA_BUNDLE");
        test = (Test) testData.getSerializable("TEST_DATA");
        setTitle("Bộ đề số " + test.getNameTest());
        startTimerCounter();
        txtOrderQuest = findViewById(R.id.txtOrderQuest);
        // this.getSupportActionBar().setTitle("Bộ đề số " + test.getNameTest());

        //copy data base if not exit
        DatabasePreparing databasePreparing = new DatabasePreparing(this);


        //o day moi goi ham List<TestQuestion> getListQuestTestFromDb(int idTest);
        testQuestionViewModel = ViewModelProviders.of(this).get(TestQuestionViewModel.class);


        //get the test question from the testId from db
        listTestQuestion = testQuestionViewModel.getTestQuestionList(test.getIdTest());

        //còn cái list này thì lấy dựa vào hàm gọi getListQuestionFromDb(List<TestQuestion> listQuestionTest);
        questionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);
        listQuestion = questionViewModel.getListQuest(listTestQuestion);

        questionPagerAdapter = new QuestionPagerAdapter(getSupportFragmentManager(), listQuestion, listTestQuestion, test);
        questionViewPager = (ViewPager) findViewById(R.id.testQuesViewPager);
        questionViewPager.setAdapter(questionPagerAdapter);
        questionViewPager.setOnPageChangeListener(pageChangeListener);

        listStatusQuestion = findViewById(R.id.listStatusQuestion);
        testStatusAdapter = new TestStatusAdapter(listQuestion, listTestQuestion);
        listStatusQuestion.setLayoutManager(new GridLayoutManager(this, 4));

        testStatusAdapter.setOnItemClickListener(new TestStatusAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                questionViewPager.setCurrentItem(position);
            }
        });
        listStatusQuestion.setAdapter(testStatusAdapter);
        testList = ((AppCompatActivity) this).findViewById(R.id.testRecyclerView);
        // RecyclerView testRecyclerView = (RecyclerView)findViewById(R.id.testRecyclerView);


    }


    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {
            questionPagerAdapter.getItem(currentPosition).onPause();

            questionPagerAdapter.getItem(newPosition).onResume();

            currentPosition = newPosition;
            TextView txtOrderQuest = findViewById(R.id.txtOrderQuest);
            txtOrderQuest.setText((currentPosition + 1) + "/25");

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) { }

        public void onPageScrollStateChanged(int arg0) { }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.do_test, menu);
        if (test.isFinish()) {
            menu.getItem(0).setTitle("Làm lại");
        } else {
            menu.getItem(0).setTitle("Kết thúc");
        }
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.doTest:
                if (test.isFinish()) {
                    beginTest();
                    item.setTitle("Kết thúc");
                } else {
                    endTest();
                    item.setTitle("Làm lại");
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void beginTest() {

        //reset test status
        test.setFinish(false);
        test.setFailed(false);
        test.setTotalSuccess(0);
        test.setCurrentTime(30000);

        //reset testquestion status
        listTestQuestion = testQuestionViewModel.resetTestQuest();
        //chỗ này làm sao để thông báo cho người ta biết là cái testquest có thay đổi nè
         testStatusAdapter.notifyDataSetChanged();

        //reset all question
        questionViewPager.setAdapter(questionPagerAdapter);
        //notify the data change
        RecyclerView answerList = findViewById(R.id.listAnswer);
        answerList.getAdapter().notifyDataSetChanged();
        questionViewPager.setCurrentItem(0);
        txtOrderQuest.setText("1/25");
        startTimerCounter();

    }

    private void endTest() {
        stopTimerCounter();
        test.setFinish(true);
        //set fail hay pass(xem laij ham nay)
        int numOfCorrectQuestion = test.checkFail(listQuestion, listTestQuestion);
        if (numOfCorrectQuestion >= 21) {
            test.setFailed(false);
        } else {
            test.setFailed(true);
        }
        //set number of correct question
        test.setTotalSuccess(numOfCorrectQuestion);
        //reset all question
        questionViewPager.setAdapter(questionPagerAdapter);

        //notify the data change
        RecyclerView answerList = findViewById(R.id.listAnswer);
        answerList.getAdapter().notifyDataSetChanged();
        questionViewPager.setCurrentItem(0);
        txtOrderQuest.setText("1/25");
//        Menu menu = findViewById(R.id.doTestMenu);
//        menu.getItem(0).setTitle("Làm lại");
    }

    @Override
    protected void onPause() {
        stopTimerCounter();
        super.onPause();
    }

    public void startTimerCounter() {
        if (test.getCurrentTime() - 1000 >= 0 && !test.isFinish()) {
            countDownTimer = new CountDownTimer(test.getCurrentTime(), 1000) {
                @Override
                public void onTick(long l) {
                    test.setCurrentTime((int) l);
                    //TestActivity.testAdapter.notifyItemChanged(test.getIdTest() - 1);
                    updateTimer(test.getCurrentTime());
                    isCountTimerRunning = true;
                }

                @Override
                public void onFinish() {
                    endTest();
                    isCountTimerRunning = false;
                }
            }.start();
        } else {
            updateTimer(test.getCurrentTime());
        }
    }

    public void stopTimerCounter() {
        if (isCountTimerRunning) {
            countDownTimer.cancel();
            isCountTimerRunning = false;
        }
        TestDatabaseAccess testDatabaseAccess = TestDatabaseAccess.getInstance(doTestActivity);
        testDatabaseAccess.open();
        testDatabaseAccess.updateTest(test);
        testDatabaseAccess.close();

    }

    public void updateTimer(int timeLeftMiniSecond) {
        int minu = (int) (timeLeftMiniSecond / 60000);
        int second = (int) (timeLeftMiniSecond % 60000 / 1000);

        String timer = "" + minu;
        timer += ":";
        if (second < 10) {
            timer += "0";
        }
        timer += second;
        TextView txtTimer = findViewById(R.id.txtTimer);
        txtTimer.setText(timer);
    }


}