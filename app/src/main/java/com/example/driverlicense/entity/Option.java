package com.example.driverlicense.entity;

public class Option {
    private String order;
    private String contentOption;
    private boolean isSelected;
    private boolean isTrue;
    private boolean isImportant;

    public Option() {
    }

    public Option(String order, String contentOption) {
        this.order = order;
        this.contentOption = contentOption;
        this.isSelected = isSelected;
        this.isTrue = isTrue;
    }

    public Option(String order, String contentOption, boolean isSelected, boolean isTrue, boolean isImportant) {
        this.order = order;
        this.contentOption = contentOption;
        this.isSelected = isSelected;
        this.isTrue = isTrue;
        this.isImportant = isImportant;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getContentOption() {
        return contentOption;
    }

    public void setContentOption(String contentOption) {
        this.contentOption = contentOption;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean aTrue) {
        isTrue = aTrue;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }
}
