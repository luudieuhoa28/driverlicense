package com.example.driverlicense.entity;

import java.io.Serializable;
import java.util.List;

public class Test implements Serializable {
    private int idTest;
    private int nameTest;
    private int currentQuest;
    private String classLicense;
    private int currentTime;
    private String timeHistory;
    private int totalSuccess;
    private boolean isFinish;
    private boolean isFailed;

    public Test() {
    }

    public Test(int idTest, int nameTest, int currentQuest, String classLicense, int currentTime, String timeHistory, int totalSuccess, boolean isFinish, boolean isFailed) {
        this.idTest = idTest;
        this.nameTest = nameTest;
        this.currentQuest = currentQuest;
        this.classLicense = classLicense;
        this.currentTime = currentTime;
        this.timeHistory = timeHistory;
        this.totalSuccess = totalSuccess;
        this.isFinish = isFinish;
        this.isFailed = isFailed;
    }

    public Test(int testID, int nameTest, int currentQuest, int currentTime, boolean isFinish, boolean isFailed) {
        this.idTest = testID;
        this.nameTest = nameTest;
        this.currentQuest = currentQuest;
        this.currentTime = currentTime;
        this.isFinish = isFinish;
        this.isFailed = isFailed;
    }



    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public int getNameTest() {
        return nameTest;
    }

    public void setNameTest(int nameTest) {
        this.nameTest = nameTest;
    }

    public int getCurrentQuest() {
        return currentQuest;
    }

    public void setCurrentQuest(int currentQuest) {
        this.currentQuest = currentQuest;
    }

    public String getClassLicense() {
        return classLicense;
    }

    public void setClassLicense(String classLicense) {
        this.classLicense = classLicense;
    }

    public int getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(int currentTime) {
        this.currentTime = currentTime;
    }

    public String getTimeHistory() {
        return timeHistory;
    }

    public void setTimeHistory(String timeHistory) {
        this.timeHistory = timeHistory;
    }

    public int getTotalSuccess() {
        return totalSuccess;
    }

    public void setTotalSuccess(int totalSuccess) {
        this.totalSuccess = totalSuccess;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    public boolean isFailed() {
        return isFailed;
    }

    public void setFailed(boolean failed) {
        isFailed = failed;
    }

    public int checkFail(List<Question> questionList, List<TestQuestion> testQuestionList) {
        int numOfCorrect = 0;

        for (int i = 0; i < questionList.size(); i++) {
            if (questionList.get(i).isQuestionDie() && (questionList.get(i).getAnswer() != testQuestionList.get(i).getAnswer())) {
                return 0;
            }
            if (questionList.get(i).getAnswer() == testQuestionList.get(i).getAnswer()) {
                numOfCorrect += 1;
            }
        }
        return numOfCorrect;
    }
}
