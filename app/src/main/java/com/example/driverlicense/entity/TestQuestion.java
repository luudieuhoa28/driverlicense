package com.example.driverlicense.entity;

import java.io.Serializable;

public class TestQuestion implements Serializable {

    private int testQuesId;
    private int testId;
    private int questionId;
    private int answer;
    private int history;
    private int numberWrong;

    public TestQuestion() {
    }

    public TestQuestion(int testQuesId, int testId, int questionId, int answer, int history, int numberWrong) {
        this.testQuesId = testQuesId;
        this.testId = testId;
        this.questionId = questionId;
        this.answer = answer;
        this.history = history;
        this.numberWrong = numberWrong;
    }

    public int getTestQuesId() {
        return testQuesId;
    }

    public void setTestQuesId(int testQuesId) {
        this.testQuesId = testQuesId;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public int getHistory() {
        return history;
    }

    public void setHistory(int history) {
        this.history = history;
    }

    public int getNumberWrong() {
        return numberWrong;
    }

    public void setNumberWrong(int numberWrong) {
        this.numberWrong = numberWrong;
    }
}
