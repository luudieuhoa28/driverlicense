package com.example.driverlicense.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.driverlicense.entity.Test;
import com.example.driverlicense.entity.TestQuestion;

import java.util.ArrayList;
import java.util.List;

public class TestDatabaseAccess {
    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase db;
    private static TestDatabaseAccess instance;


    private static final String TAG = "SQLite";
    private static final String TABLE_TEST = "ZTEST";
    private static final String CLASS_LICENSE = "CLASS_LICENSE";
    private static final String COLUMN_IS_FINISH = "ISFINISH";
    private static final String COLUMN_IS_FAILED = "ISFAILED";
    private static final String COLUMN_TOTAL_SUCCESS = "TOTAL_SUCCESS";
    private static final String COLUMN_CURRENT_TIME = "CURRENT_TIME";
    private static final String COLUMN_TEST_ID = "IDTEST";

    public TestDatabaseAccess(Context context) {
        openHelper = new DatabaseOpenHelper(context);
    }

    public static TestDatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new TestDatabaseAccess(context);
        } return instance;
    }

    public void open() {
        this.db = openHelper.getWritableDatabase();
    }

    public void  close() {
        if(db != null) {
            this.db.close();
        }
    }

    public List<Test> getListTest(String classLicense) {
        List<Test> listTest = new ArrayList<>();
        Cursor cursor = null;
        try {
            Log.i(TAG, "TestDatabaseAccess.getListA1Test...");
            String query = "SELECT * FROM " + TABLE_TEST + " WHERE " + CLASS_LICENSE + " = '" + classLicense + "'";
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do{
                    Test test = new Test();
                    test.setIdTest(Integer.parseInt(cursor.getString(0)));
                    test.setNameTest(Integer.parseInt(cursor.getString(1)));
                    test.setCurrentQuest(Integer.parseInt(cursor.getString(2)));
                    test.setClassLicense(cursor.getString(3));
                    int tmpTime = Integer.parseInt(cursor.getString(4));
                    if (tmpTime == 0) {
                        test.setCurrentTime(30000);
                    }else {
                        test.setCurrentTime(tmpTime);
                    }
                    test.setTimeHistory(cursor.getString(5));
                    test.setTotalSuccess(Integer.parseInt(cursor.getString(6)));
                    test.setFinish(Integer.parseInt(cursor.getString(7)) == 1);
                    test.setFailed(Integer.parseInt(cursor.getString(8)) == 1);
                    listTest.add(test);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listTest;
    }

    //call after finish or pause() the test
    public int updateTest(Test test) {
        Log.i(TAG, "TestDatabaseAccess.updateTestQuest ... " + test.getIdTest());

        //   SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(COLUMN_IS_FINISH, test.isFinish()? 1 : 0);
        values.put(COLUMN_IS_FAILED, test.isFailed()? 1 : 0);
        values.put(COLUMN_TOTAL_SUCCESS, test.getTotalSuccess());
        values.put(COLUMN_CURRENT_TIME, test.getCurrentTime() == 0 ? 30000 : test.getCurrentTime());

        // updating row
        int result = db.update(TABLE_TEST, values, COLUMN_TEST_ID + " = ?",
                new String[]{String.valueOf(test.getIdTest())});
        return result;
    }
}
