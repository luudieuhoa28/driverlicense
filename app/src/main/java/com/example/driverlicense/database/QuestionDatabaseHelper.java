package com.example.driverlicense.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class QuestionDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "SQLite";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "600question_final";

    // Table name: Note.
    private static final String TABLE_TEST_QUESTION = "ZTESTQUEST";

    private static final String COLUMN_QUESTION_ID ="Z_PK";
    private static final String COLUMN_CONTENT ="ZTESTQUESIONCONTENT";
    private static final String COLUMN_IMAGE = "ZIMAGE";
    private static final String COLUMN_OPTION_1 = "ZOPTION1";
    private static final String COLUMN_OPTION_2 = "ZOPTION2";
    private static final String COLUMN_OPTION_3 = "ZOPTION3";
    private static final String COLUMN_OPTION_4 = "ZOPTION4";
    private static final String COLUMN_ANSWER_DESC = "ZANSWERDESC";
    private static final String COLUMN_ANSWER = "ZANSWER";
    private static final String COLUMN_IS_LEARNED = "ZLEARNED";
    private static final String COLUMN_IS_MARKED = "ZMARKED";
    private static final String COLUMN_IS_WRONG = "ZWRONG";

    public QuestionDatabaseHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    public QuestionDatabaseHelper(Context context)  {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
