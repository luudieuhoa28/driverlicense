package com.example.driverlicense.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.driverlicense.entity.TestQuestion;

import java.util.ArrayList;
import java.util.List;

public class TestQuestionDatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static TestQuestionDatabaseAccess instance;

    private static final String TAG = "SQLite";
    private static final String TABLE_TEST_QUESTION = "ZTESTQUEST";
    private static final String COLUMN_TEST_QUEST_ID = "ZTESTQUESTID";
    private static final String COLUMN_TEST_ID = "TESTID";
    private static final String COLUMN_QUESTION_ID = "ZQUESTIONID";
    private static final String COLUMN_ANSWER = "ZANSWER";
    private static final String COLUMN_HISTORY = "ZHISTORY";
    private static final String COLUMN_NUMBER_WRONG = "ZNUMBERWRONG";

    private TestQuestionDatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    public static TestQuestionDatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new TestQuestionDatabaseAccess(context);
        }
        return instance;
    }

    public void open() {
        this.db = openHelper.getWritableDatabase();
    }

    public void close() {
        if (db != null) {
            this.db.close();
        }
    }

    public List<TestQuestion> getTestQuestionById(int testId) {
        List<TestQuestion> testQuestionList = new ArrayList<TestQuestion>();
        Cursor cursor = null;
        try {
            Log.i(TAG, "TestQuestionDatabaseAccess.getTestQuestionById ... ");


            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_TEST_QUESTION + " WHERE " + COLUMN_TEST_ID + " = " + testId;
            //  openDatabase();
            // String selectQuery = "SELECT  * FROM " + TABLE_TEST_QUESTION;
            cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    TestQuestion testQuestion = new TestQuestion();
                    testQuestion.setTestQuesId(Integer.parseInt(cursor.getString(0)));
                    testQuestion.setTestId(Integer.parseInt(cursor.getString(1)));
                    testQuestion.setQuestionId(Integer.parseInt(cursor.getString(2)));
                    testQuestion.setAnswer(0);
                    try {
                        testQuestion.setAnswer(Integer.parseInt(cursor.getString(3)));
                    } catch (Exception e) {
                    }
                    testQuestion.setHistory(0);
                    try{
                        testQuestion.setHistory(Integer.parseInt(cursor.getString(4)));
                    } catch (Exception e) {

                    }
                    testQuestion.setNumberWrong(Integer.parseInt(cursor.getString(5)));

                    // Adding note to list
                    testQuestionList.add(testQuestion);
                } while (cursor.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        cursor.close();
        // return note list
        return testQuestionList;
    }

    //call after user choose an option during test
    public int updateTestQuest(TestQuestion testQuestion) {
        Log.i(TAG, "TestQuestionDatabaseAccess.updateTestQuest ... " + testQuestion.getTestQuesId());

        //   SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ANSWER, testQuestion.getAnswer());

        // updating row
        return db.update(TABLE_TEST_QUESTION, values, COLUMN_TEST_QUEST_ID + " = ?",
                new String[]{String.valueOf(testQuestion.getTestQuesId())});
    }

    //call if want to do a new test
    public void resetTestQuest(List<TestQuestion> testQuestionList) {
        Log.i(TAG, "TestQuestionDatabaseAccess.resetTestQuest ... ");

        //   SQLiteDatabase db = this.getWritableDatabase();
        for (TestQuestion testQuestion: testQuestionList) {
            ContentValues values = new ContentValues();
            testQuestion.setAnswer(0);
            values.put(COLUMN_ANSWER, 0);
            db.update(TABLE_TEST_QUESTION, values, COLUMN_TEST_QUEST_ID + " = ?",
                    new String[]{String.valueOf(testQuestion.getTestQuesId())});
        }
    }
}
