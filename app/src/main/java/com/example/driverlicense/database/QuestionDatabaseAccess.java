package com.example.driverlicense.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.driverlicense.entity.Question;
import com.example.driverlicense.entity.TestQuestion;

import java.util.ArrayList;
import java.util.List;

public class QuestionDatabaseAccess {
    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase db;
    private static QuestionDatabaseAccess instance;

    private static final String TAG = "SQLite";
    private static final String TABLE_QUESTION = "ZQUESTION";
    private static final String PK = "Z_PK";

    public QuestionDatabaseAccess (Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    public static QuestionDatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new QuestionDatabaseAccess(context);
        } return instance;
    }


    public void open() {
        this.db = openHelper.getWritableDatabase();
    }

    public void  close() {
        if(db != null) {
            this.db.close();
        }
    }

    public List<Question> getListQuestion(List<TestQuestion> testQuestionList) {
        List<Question> questionList = new ArrayList<>();
        Cursor cursor = null;
        try{
            for (TestQuestion testQuestion: testQuestionList) {
                String query = "SELECT * FROM " + TABLE_QUESTION + " WHERE " + PK + " = " + testQuestion.getQuestionId();
                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    Question question = new Question();
                    question.setQuestionId(Integer.parseInt(cursor.getString(0)));
                    question.setQuestionContent(cursor.getString(1));
                    question.setQuestionImg(cursor.getString(2));
                    question.setOption1(cursor.getString(3));
                    question.setOption2(cursor.getString(4));
                    question.setOption3(cursor.getString(5));
                    question.setOption4(cursor.getString(6));
                    question.setAnswerDescription(cursor.getString(7));
                    question.setAnswer(Integer.parseInt(cursor.getString(8)));
                    question.setWrong(Integer.parseInt(cursor.getString(12)) == 1);
                    if (cursor.getString(20) != null) {
                        question.setQuestionDie(Integer.parseInt(cursor.getString(20)) == 1);
                    } else {
                        question.setQuestionDie(false);
                    }

                    questionList.add(question);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionList;
    }
}
