package com.example.driverlicense.adapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.driverlicense.entity.Question;
import com.example.driverlicense.entity.Test;
import com.example.driverlicense.entity.TestQuestion;
import com.example.driverlicense.fragment.QuestionFragment;

import java.util.ArrayList;
import java.util.List;

//This adapter is use for get every page in the ViewPager which is create by QuestionFragment
public class QuestionPagerAdapter extends FragmentPagerAdapter {
    private static List<Question> listQuestion = new ArrayList<>();
    private static List<TestQuestion> listTestQuestion = new ArrayList<>();
    private Test test;


    public QuestionPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    public QuestionPagerAdapter(@NonNull FragmentManager fm, List<Question> listQuestion, List<TestQuestion> listTestQuestion, Test test) {
        super(fm);
        this.listQuestion = listQuestion;
        this.listTestQuestion = listTestQuestion;
        this.test = test;
    }


    public static List<TestQuestion> getListTestQuestion() {
        return listTestQuestion;
    }

    public static void setListTestQuestion(List<TestQuestion> listTestQuestion) {
        QuestionPagerAdapter.listTestQuestion = listTestQuestion;
    }

    public static List<Question> getListQuestion() {
        return listQuestion;
    }

    public void setListQuestion(List<Question> listQuestion) {
        this.listQuestion = listQuestion;
    }

    //ở phần này, nếu cái getposition không chính xác
    //thì ở đây gọi 1 hàm để lấy được testquest theo id của question tại vị trí position
    @NonNull
    @Override
    public Fragment getItem(int position) {
     //  new AppCompatActivity().getSupportActionBar().setTitle((position + 1) + "/25");
        return QuestionFragment.newInstance(listQuestion.get(position), listTestQuestion.get(position), position, test);
    }

    @Override
    public int getCount() {
        return listTestQuestion.size();
    }
}
